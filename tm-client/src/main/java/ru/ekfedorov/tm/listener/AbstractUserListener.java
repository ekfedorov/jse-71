package ru.ekfedorov.tm.listener;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import ru.ekfedorov.tm.endpoint.AdminUserEndpoint;
import ru.ekfedorov.tm.endpoint.SessionEndpoint;
import ru.ekfedorov.tm.endpoint.UserEndpoint;

public abstract class AbstractUserListener extends AbstractListener {

    @NotNull
    @Autowired
    protected UserEndpoint userEndpoint;

    @NotNull
    @Autowired
    protected AdminUserEndpoint adminUserEndpoint;

    @NotNull
    @Autowired
    protected SessionEndpoint sessionEndpoint;

}
