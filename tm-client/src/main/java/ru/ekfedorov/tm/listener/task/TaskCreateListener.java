package ru.ekfedorov.tm.listener.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.ekfedorov.tm.event.ConsoleEvent;
import ru.ekfedorov.tm.listener.AbstractTaskListener;
import ru.ekfedorov.tm.endpoint.Session;
import ru.ekfedorov.tm.exception.system.NullObjectException;
import ru.ekfedorov.tm.util.TerminalUtil;

import static ru.ekfedorov.tm.util.ValidateUtil.isEmpty;

@Component
public final class TaskCreateListener extends AbstractTaskListener {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Create new task.";
    }

    @NotNull
    @Override
    public String commandName() {
        return "task-create";
    }

    @SneakyThrows
    @Override
    @EventListener(condition = "@taskCreateListener.commandName() == #event.name")
    public void handler(@NotNull ConsoleEvent event) {
        if (sessionService == null) throw new NullObjectException();
        @Nullable final Session session = sessionService.getSession();
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        @NotNull String name = TerminalUtil.nextLine();
        if (isEmpty(name)) name = "task";
        System.out.println("ENTER DESCRIPTION:");
        @NotNull String description = TerminalUtil.nextLine();
        if (isEmpty(description)) description = "without description";
        taskEndpoint.addTask(session, name, description);
        System.out.println();
    }

}
