package ru.ekfedorov.tm.client.soap;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.junit.*;
import org.junit.experimental.categories.Category;
import org.springframework.http.HttpHeaders;
import ru.ekfedorov.tm.api.endpoint.IAuthEndpoint;
import ru.ekfedorov.tm.api.endpoint.ITaskEndpoint;
import ru.ekfedorov.tm.client.soap.AuthClient;
import ru.ekfedorov.tm.client.soap.TaskClient;
import ru.ekfedorov.tm.marker.WebCategory;
import ru.ekfedorov.tm.model.Task;

import javax.xml.ws.BindingProvider;
import javax.xml.ws.handler.MessageContext;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class TaskEndpointTest {

    private final Task task = new Task("test", "");

    private final Task task2 = new Task("test2", "");

    @NotNull
    private static IAuthEndpoint authEndpoint;

    @NotNull
    private static ITaskEndpoint endpoint;

    @BeforeClass
    @SneakyThrows
    public static void beforeClass() {
        authEndpoint = AuthClient.getInstance("http://localhost:8080");
        Assert.assertTrue(
                authEndpoint.login("user", "user").getSuccess()
        );
        endpoint = TaskClient.getInstance("http://localhost:8080");
        setSession();
    }

    private static void setSession() throws MalformedURLException {
        @NotNull final BindingProvider bindingProvider =
                (BindingProvider) authEndpoint;
        @NotNull final ObjectMapper oMapper = new ObjectMapper();
        @NotNull final Object headers = bindingProvider.getResponseContext()
                .get(MessageContext.HTTP_RESPONSE_HEADERS);
        @NotNull final Map<String, String> mapHeaders =
                oMapper.convertValue(headers, Map.class);
        @NotNull final Object cookieValue =
                mapHeaders.get(HttpHeaders.SET_COOKIE);
        @NotNull final List<String> collection = (List<String>) cookieValue;
        ((BindingProvider) endpoint).getRequestContext().put(
                MessageContext.HTTP_REQUEST_HEADERS,
                Collections.singletonMap(
                        "Cookie",
                        Collections.singletonList(collection.get(0))
                )
        );
    }

    @Before
    public void before() {
        endpoint.create(task);
    }

    @After
    public void after() {
        endpoint.deleteAll();
    }

    @Test
    @Category(WebCategory.class)
    public void find() {
        Assert.assertEquals(task.getName(), endpoint.find(task.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void create() {
        Assert.assertNotNull(endpoint.find(task.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void update() {
        final Task updatedTask = endpoint.find(task.getId());
        updatedTask.setName("updated");
        endpoint.save(updatedTask);
        Assert.assertEquals("updated", endpoint.find(task.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void delete() {
        endpoint.delete(task.getId());
        Assert.assertNull(endpoint.find(task.getId()));
    }

    @Test
    @Category(WebCategory.class)
    public void findAll() {
        Assert.assertEquals(1, endpoint.findAll().size());
        endpoint.create(task2);
        Assert.assertEquals(2, endpoint.findAll().size());
    }

    @Test
    @Category(WebCategory.class)
    public void updateAll() {
        endpoint.create(task2);
        final Task updatedTask = endpoint.find(task.getId());
        updatedTask.setName("updated1");
        final Task updatedTask2 = endpoint.find(task2.getId());
        updatedTask2.setName("updated2");
        List<Task> updatedList = new ArrayList<>();
        updatedList.add(updatedTask);
        updatedList.add(updatedTask2);
        endpoint.saveAll(updatedList);
        Assert.assertEquals("updated1", endpoint.find(task.getId()).getName());
        Assert.assertEquals("updated2", endpoint.find(task2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void createAll() {
        endpoint.delete(task.getId());
        List<Task> list = new ArrayList<>();
        list.add(task);
        list.add(task2);
        endpoint.createAll(list);
        Assert.assertEquals("test", endpoint.find(task.getId()).getName());
        Assert.assertEquals("test2", endpoint.find(task2.getId()).getName());
    }

    @Test
    @Category(WebCategory.class)
    public void deleteAll() {
        endpoint.create(task2);
        Assert.assertEquals(2, endpoint.findAll().size());
        endpoint.deleteAll();
        Assert.assertNull(endpoint.findAll());
    }

}
