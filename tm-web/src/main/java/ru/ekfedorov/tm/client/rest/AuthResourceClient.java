package ru.ekfedorov.tm.client.rest;

import feign.Feign;
import okhttp3.JavaNetCookieJar;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ekfedorov.tm.model.AuthorizedUser;
import ru.ekfedorov.tm.model.Result;
import ru.ekfedorov.tm.resource.AuthResource;

import java.net.CookieManager;
import java.net.CookiePolicy;

public interface AuthResourceClient {

    static AuthResource client(final String base) {
        final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        final HttpMessageConverters converters = new HttpMessageConverters(converter);
        final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;

        final CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);

        final okhttp3.OkHttpClient.Builder builder = new okhttp3.OkHttpClient().newBuilder();
        builder.cookieJar(new JavaNetCookieJar(cookieManager));

        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(AuthResource.class, base);
    }

    @GetMapping("/login")
    public Result login(
            @RequestParam("username") final String username,
            @RequestParam("password") final String password
    );

    @GetMapping("/session")
    public Authentication session();

    @GetMapping("/profile")
    public AuthorizedUser user(@AuthenticationPrincipal(errorOnInvalidType = true) final AuthorizedUser user);

    @GetMapping("/logout")
    public Result logout();

}
