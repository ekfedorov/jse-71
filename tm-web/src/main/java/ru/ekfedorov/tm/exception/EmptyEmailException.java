package ru.ekfedorov.tm.exception;

import org.jetbrains.annotations.NotNull;

public class EmptyEmailException extends AbstractException {

    @NotNull
    public EmptyEmailException() {
        super("Error. Email is empty");
    }

}
