package ru.ekfedorov.tm.exception;

import org.jetbrains.annotations.NotNull;

public class UserNotFoundException extends AbstractException {

    @NotNull
    public UserNotFoundException() {
        super("Error. User not found.");
    }

}
